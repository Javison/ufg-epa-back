UFirstgroup - Backend Assesment
===============

This repository contains code for **UFirstgroup assesment task 1** witch requires:<br>
> Write a script that imports the access logfile and creates a ​new​ file that holds the log data structured as a JSON-Array.

<br><br>

Table of Contents
-----------------

- [Getting Started](#getting-started)
- [Project Structure](#project-structure)
- [List of Packages](#list-of-packages)
- [Run locally](#run-locally)
- [Deploy](#deploy)
- [License](#license)

<br><br>


Getting Started
---------------


### Requirements

- Node.js 10. [link setup](https://nodejs.org/en/)
- npm (With Node)
- git version control system. [link setup](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- firebase-tools. [link setup](https://firebase.google.com/docs/web/setup)

<br>

### Clone repository & install

Download the code:

```bash
# Get the latest snapshot
git clone --depth=1 https://Javison@bitbucket.org/Javison/ufg-epa-back.git ufg-epa-back

# Change directory
cd ufg-epa-back

# Install NPM dependencies
npm install
```

Install toolkit and access your account if you are going to use backend services:

```bash
$ npm install -g firebase-tools
$ firebase login
# You must receive project acces after login
$ firebase projects:list
$ firebase use ufg-epa
```


#### Proxy settings

Add proxy:

```bash
# npm
npm config set proxy http://userproxy:passwordproxy@proxy.company.es:8080
npm config set https-proxy http://userproxy:passwordproxy@proxy.company.es:8080
# git
git config --global http.proxy http://userproxy:passwordproxy@proxy.company.es:8080
git config --global https.proxy https://userproxy:passwordproxy@proxy.company.es:8080
```

Remove proxy:

```bash
# npm
npm config rm proxy
npm config rm https-proxy
# git
git config --global --unset http.proxy
git config --global --get http.proxy https://userproxy:passwordproxy@proxy.company.es:8080
```

<br><br>


Project Structure
-----------------

| Name                               | Description                                                  |
| ---------------------------------- | ------------------------------------------------------------ |
| **lib**                            | Generated compiled code by tsc that will be deploy           |
| **src/***                          | Root code folder                                             |
| **src/assets**                     | Images and other static files                                |
| **src/fn**                         | Main folder for independent cloud functions                  |
| **src/helpers**                    | Static methods for different purpose.                        |
| **src/interfaces**                 | Data model descriptions                                      |
| **src/models**                     | Data classes to implement                                    |
| **src/tests**                      | Testing classes                                              |
| **src/index.ts**                   | Main file to group Cloud Functions                           |
| **node_modules**/                  | npm dependency packages                                      |
| .gitignore                         | Git ignore config.                                           |
| firebase.json                      | Firebase configuration file                                  |
| package.json                       | NPM dependencies.                                            |
| serviceAccountKey.json             | Server keys from IAM to access Google Cloud                  |
| tsconfig.json                      | Typescript configuration file                                |
| tslint.json                        | TypeScript analysis tool that checks code for readability    |

<br><br>


List of Packages
----------------
### Dependencies

| Package                         | Description                                                           |
| ------------------------------- | --------------------------------------------------------------------- |
| firebase-admin                  | Firebase admin tools to access database, storage, etc...              |
| firebase-functions              | Integrates the Firebase platform with cloud functions in nodejs       |
| @google-cloud/storage           | Google Cloud storage and retrieval of any amount of data              |
| mkdirp                          | Manage directories in nodejs                                          |
| moment-timezone                 | Date library for parsing, validating, manipulating, and formatting    |


### Development Dependencies

| Package                         | Description                                                           |
| ------------------------------- | --------------------------------------------------------------------- |
| @types/xx                       | Type description for typescript                                       |
| typescript                      | Typescript compiler                                                   |
| firebase-functions-test         | Firebase emulator suite                                               |
| jest                            | Javascript testing                                                    |
| ts-*                            | Suport for typescript                                                 |

<br><br>


Run locally
--------

To make use of storage bucket you need [Google Cloud Platform](https://console.cloud.google.com/iam-admin) project access. 
Afterwards export your "serviceAccountKey.json" and place in root folder.<br>

Following commands will: <br> 
1. Compile ts code and watch for changes
2. Start emulator and load database skeleton

```bash
# Terminal 1: Typescript compile and watch for changes
$ tsc --watch
# Terminal 2: Start database and nodejs cloud functions
$ npm run serve:local 
# Same as: firebase emulators:start --import=./src/tests/config/emulators
```

Once the database local server is awake you can use postman for request. ie:
> GET http://localhost:9000/app/server_analysis/etl/staging/data.json?ns=ufg-epa

```bash
# Terminal 1:
$ curl http://localhost:9000/app/server_analysis/etl/staging/data.json?ns=ufg-epa
```


Since there is only one function to execute locally (Triggered from database), you could either:
- Run front-end app (ng serve) and [upload a new file](http://localhost:4200/upload)
- Duplicate any of the upload entries from [emulator UI](http://localhost:4000/database/ufg-epa/data/app/server_analysis/etl/source/data/uploads) 
- Insert json with postman POST request 
- Run shell (npm run shell), and call function with json entry params

<br><br>


Deploy
--------

```bash
# Firebase login (Must have access to project)
$ firebase login

# Select the project
$ firebase projects:list
$ firebase use ufg-epa

# Build
$ npm run build

# Deploy
$ firebase deploy
```

<br><br>


Coding recommendations
---------------

### Minimizing cold start time
Implemented architecture to reduce cold start time in serverless environments. 
More info in this [post](https://medium.com/firebase-developers/organize-cloud-functions-for-max-cold-start-performance-and-readability-with-typescript-and-9261ee8450f0)
and [github](https://github.com/CodingDoug/min-functions-cold-start/).

<br>

### Standarts
- Write comments and documentation
- Write readable yet efficient code
- Write tests
- SOLID principles (more info)[https://en.wikipedia.org/wiki/SOLID]

<br><br>


License
-------

Copyright (c) 2020 Javier Gracia Muro