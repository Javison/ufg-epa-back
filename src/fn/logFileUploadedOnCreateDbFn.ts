/**
 * ETL process to convert txt epa server logs in json file
 * Logs collected by Laura Bottomley (​laurab@ee.duke.edu)​ of Duke University.
 */
import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin';
import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';
import * as rd from 'readline';
import * as mkdirp from 'mkdirp';

import { Storage, GetSignedUrlConfig, File } from '@google-cloud/storage';

import LogFileParser from '../helpers/log-file-parser.helper';
import { LogRecord } from '../interfaces/log-record.interface';
import { LogFileTxtMetadata } from '../interfaces/log-file-txt-metadata.interface';

const storage = new Storage({ 
    keyFilename: "serviceAccountKey.json"
});

admin.initializeApp();

const STORAGE_BUCKET = 'ufg-epa.appspot.com';
const DB_PATH_UPLOADS = '/app/server_analysis/etl/source/data/uploads';
const STORAGE_PATH_STG_FILES = 'server_analysis/stg_files';

const ETL_STATUS_LOAD = 'load';
const ETL_STATUS_ERROR = 'error';
const ETL_STATUS_TRANSFORM = 'transform';
const ETL_STATUS_COMPLETE = 'complete';

/**
 * Function triggered when a new txt log file is uploaded and a 
 * reference is created in database path: /app/server_analysis/etl/source/data/uploads/{fileProcessId}
 */
export default async (snapshot: functions.database.DataSnapshot, context: functions.EventContext) => {
    console.log('logFileUploadedOnCreateDbFn START');
    
    const logFileTxtMetadataId = snapshot.key;
    const logFileTxtMetadata: LogFileTxtMetadata = snapshot.val();
    console.log('logFileUploadedOnCreateDbFn > logFileTxtMetadata', logFileTxtMetadataId, logFileTxtMetadata);

    // Notify user transformation started
    await admin.database().ref(`${DB_PATH_UPLOADS}/${logFileTxtMetadataId}/transformStatus`).set(ETL_STATUS_TRANSFORM);

    try {
        // Read file from Google Cloud storage and parse lines to json object
        const logFile = storage.bucket(`${STORAGE_BUCKET}`).file(logFileTxtMetadata.storagePath.substring(1)); // Remove first slash / from path. 'epa-http-short.txt'
        const logRecords: LogRecord[] = await readFileStream(logFile);
        console.log('logFileUploadedOnCreateDbFn > logRecords.length:', logRecords.length);

        // Create json file in google cloud & notify user
        await admin.database().ref(`${DB_PATH_UPLOADS}/${logFileTxtMetadataId}/transformStatus`).set(ETL_STATUS_LOAD);
        const url = await createJsonFile(logRecords, logFileTxtMetadata.fileName);

        // Update url & notify
        admin.database().ref(`${DB_PATH_UPLOADS}/${logFileTxtMetadataId}/urlJsonTransformedFile`).set(url);
        return admin.database().ref(`${DB_PATH_UPLOADS}/${logFileTxtMetadataId}/transformStatus`).set(ETL_STATUS_COMPLETE);

    } catch (err) {
        console.error('Error processing file: ', err);
        return admin.database().ref(`${DB_PATH_UPLOADS}/${logFileTxtMetadataId}/transformStatus`).set(ETL_STATUS_ERROR);
    }
}

/**
 * Creates a json file from logRecords object
 * 
 * @param logRecords array objects with parsed lines from txt
 * @param fileName Original file name
 */
async function createJsonFile(logRecords: LogRecord[], fileName: string) {
    console.log('createJsonFile START');
    
    const jsonFileName = `${fileName}.json`;
    const tempLocalFile = path.join(os.tmpdir(), jsonFileName);
    const tempLocalDir = path.dirname(tempLocalFile);
    await mkdirp(tempLocalDir);
    
    // Create json file in temp folder
    fs.writeFileSync(tempLocalFile, JSON.stringify(logRecords));

    // Upload json file to Cloud Storage
    await storage.bucket(`${STORAGE_BUCKET}`).upload(tempLocalFile, {
        destination: `${STORAGE_PATH_STG_FILES}/${jsonFileName}`,
    });

    // Delete the local files to free up disk space
    fs.unlinkSync(tempLocalFile);

    // Get Cloud Storage file URL
    const options: GetSignedUrlConfig = {
        version: 'v2',                            // default value
        action: 'read',                           // read | write | delete | resumable
        expires: '03-01-2500'                     // expire date. U wont be there :\
    };
    const logFileJson = storage.bucket(`${STORAGE_BUCKET}`).file(`${STORAGE_PATH_STG_FILES}/${jsonFileName}`);
    const [url] = await logFileJson.getSignedUrl(options);
    console.log('createJsonFile > logFileJson.getSignedUrl: ', url);
    return url;
}

/**
 * Read txt file line by line
 * 
 * @param logFile txt file object in storage to parse
 */
async function readFileStream(logFile: File): Promise<LogRecord[]> {
    console.log('readFileStream START');  

    return new Promise( (resolve, reject) => {
        
        const reader = rd.createInterface(logFile.createReadStream());
        let logRecords: LogRecord[] = [];
        let lineCount: number = 0;
        
        reader.on("line", (line: string) => {
            lineCount++;
            if (line && line.trim() && LogFileParser.isLogLine(line)) {
                try {
                    const logRecord: LogRecord = LogFileParser.getParsedLogLine(line);
                    logRecords.push(logRecord);
                } catch (err) {
                    console.error('Error parsing line: ', line);
                }                
            }
        });
        reader.on("close", () => {
            console.log(`** ${lineCount} Lines Readed & ${logRecords.length} processed elements to json **`);   
            resolve(logRecords);
        });
    })
}