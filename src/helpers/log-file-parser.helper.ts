import { LogDatetime } from '../interfaces/log-datetime.interface';
import { HTTPRequest } from '../interfaces/http-request.interface';
import { LogRecord } from '../interfaces/log-record.interface';
import * as moment from 'moment-timezone';


/**
 * Utils statics methods to parse log string
 * Sample line:
 * 152.35.128.13 [30:15:17:12] "GET /docs/CSO.html HTTP/1.0" 200 3120
 */
export default class LogFileParser {

    /**
     * 
     * @param line log line to parse
     */
    static getParsedLogLine(line: string): LogRecord {
        line = line.trim();

        // 1: host: Address URL or IP,         
        const host: string = LogFileParser.getHost(line);

        // 2: datetime: timestamp "[DD:HH:MM:SS]" EDT time (four hours behind GMT)
        const logDatetime: LogDatetime = LogFileParser.getLogdatetime(line);

        // 3: request: "GET /Consumer.html HTTP/1.0"
        const httpRequest: HTTPRequest = LogFileParser.getLogHttpRequest(line); 

        // 4: response_code: 200, 400,
        const response_code: number = LogFileParser.getHttpResponseCode(line);

        // 5: document_size: 6829, 7005,
        const document_size: number = LogFileParser.getServedDocumentSize(line); 

        // Create log record
        const logRecord: LogRecord = {
            host: host,
            datetime: logDatetime,
            request: httpRequest,
            response_code: response_code,
            document_size: document_size,
            // logline: line
        }

        return logRecord;
    }

    /**
     * Analyze line to check if fulfill minimun requirements according to Common log line format.
     * For this analysis, we require date and cliente request
     * More info at:
     * https://en.wikipedia.org/wiki/Common_Log_Format
     * ie.: 127.0.0.1 user-identifier frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326
     * 
     * @param line log to analyze
     * @returns true if the log line contains minumun required parameters
     */
    static isLogLine(line: string): boolean {
        // Date between brackets []
        if (!line.match("\\[.*]")
            || line.substring(line.indexOf('[') + 1, line.indexOf(']')).split(':').length < 4) {
            return false;
        }
        
        // Request from client on quotes ""
        if (!line.match(/".*?"/g)) {
            return false;
        }
        return true;
    }
            
    /**
     * 
     * @param line log line to parse
     * @returns host ip or url
     */
    static getHost(line: string): string {
        const host: string = line.substring(0, line.indexOf(' '));
        return host;
    }

    /**
     * Extract date content inside string [DD:HH:MM:SS]
     * Server located at Research Triangle Park, NC. Timezone: EDT, Durham, North Carolina, USA. UTC -4h
     * @param line log to parse
     */
    static getLogdatetime(line: string): LogDatetime {

        const datetime: string = line.substring(line.indexOf('[') + 1, line.indexOf(']'));
        const datetimeArr: string[] = datetime.split(':');
        
        // Example log: [30:15:17:12] = 1995-08-30 15:17:12 tz -04:00
        const dateStr: string = `1995-08-${datetimeArr[0]} ${datetimeArr[1]}:${datetimeArr[2]}:${datetimeArr[3]}`;
        const momentServerDate = moment(`${dateStr}-04:00`);// ~~  .tz('America/New_York') || .utcOffset(-4);
                        
        const logDatetime: LogDatetime = {
            year: momentServerDate.parseZone().get('year'),
            month: momentServerDate.parseZone().get('month'),     // 0 to 11 !!!
            day: momentServerDate.parseZone().get('date'),
            hour: momentServerDate.parseZone().get('hour'),
            minute: momentServerDate.parseZone().get('minute'),
            second: momentServerDate.parseZone().get('second'),
            timestamp: momentServerDate.parseZone().toISOString(true)
        }

        return logDatetime;
    }

    /**
     * Extract HttpRequest class
     * @param line log line to parse
     * @returns HttpRequest 
     */
    static getLogHttpRequest(line: string): HTTPRequest {
        
        const request: HTTPRequest = {
            method: 'invalid',
            url: '/',
            protocol: '',
            protocol_version: ''
        }

        try {  
            // Extract between quotes and split to array: "GET /Consumer.html HTTP/1.0"
            const requestArr: string[] = line.substring(line.indexOf('"') + 1, line.lastIndexOf('"')).split(' ');

            if (requestArr.length > 2) {
                request.method = requestArr[0];
                request.url = requestArr[1];
                request.protocol = requestArr[2].split('/')[0];
                request.protocol_version = requestArr[2].split('/')[1]

            } else {
                if (requestArr[0].match('GET') || requestArr[0].match('HEAD') 
                    || requestArr[0].match('POST') || requestArr[0].match('PUT') 
                    || requestArr[0].match('DELETE') || requestArr[0].match('CONNECT') 
                    || requestArr[0].match('OPTIONS') || requestArr[0].match('TRACE') && requestArr[0].match('PATCH')) {

                    request.method = requestArr[0];

                } else {
                    console.info('HTTPRequest does not have request.method: ', line);
                }
            }

        } catch (error) {
            console.error('Error parsing HttpRequest: ', line);
        }

        return request;
    }

    /**
     * 
     * @param line log line to parse
     * @returns Http response code (200, 400, 404, etc..)
     */
    static getHttpResponseCode(line: string): number {
        let responseCode: number = 0;
        try {
            const splittedStr: string[] = line.substring(line.lastIndexOf('"') + 1).split(' ');
            // Get the second last from array: ie: ' ,200,3120'
            responseCode = parseInt(splittedStr[splittedStr.length - 2]) || 0;

        } catch (error) {
            console.warn('Error parsing HttpResponseCode: ', line);
        }

        return responseCode
    }

    /**
     * 
     * @param line log line to parse
     * @returns Delivered document size in bytes
     */
    static getServedDocumentSize(line: string): number {

        let documentSize: number = 0;
        try {
            // Return 0 if not a number
            documentSize = parseInt(line.substring(line.lastIndexOf(' '))) || 0;
            
        } catch (error) {
            console.warn('Error parsing DocumentSize: ', line);
        }
        return documentSize
    }

}