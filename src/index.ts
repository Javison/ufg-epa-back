import * as functions from 'firebase-functions';


/**
 * Database trigger wich process log file saved in Google Storage and creates a json file
 */
export const logFileUploadedOnCreateDbFn =
  functions.database.ref('/app/server_analysis/etl/source/data/uploads/{fileProcessId}').onCreate(async (snapshot, context) => {
      await (await import('./fn/logFileUploadedOnCreateDbFn')).default(snapshot, context)
})
