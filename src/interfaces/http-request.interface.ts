/**        
 3: request: "GET /Consumer.html HTTP/1.0"
    3.1: request.method: 
    3.2: request.url: 
    3.3: request.protocol: 
    3.4: request.protocol_version: 
*/
export interface HTTPRequest {
    method: string;
    url: string;
    protocol: string;
    protocol_version: string;
}