/* 
Every log line should have: 
2: datetime: timestamp "[DD:HH:MM:SS]" EDT time (four hours behind GMT)
    2.1: request.day
    2.2: request.hour
    2.3: request.minute
    2.4: request.second 
    2.5: request.timestamp 
*/
export interface LogDatetime {
    year: number,
    month: number;
    day: number;
    hour: number;
    minute: number;
    second: number;
    timestamp: string;
}