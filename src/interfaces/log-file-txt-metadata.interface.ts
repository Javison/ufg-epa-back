

/**
 * 
 */
export interface LogFileTxtMetadata {  
    id: string;
    url: string;
    tags: string;
    fileName: string;
    isActive: boolean;
    storagePath: string;
    transformStatus: string;
    urlJsonTransformedFile: string;
  }