
/*
View / Metrics / Dimensions / Facts:
    requests.total: Total requests 47.748
    requests.get: GET requests 46.014
    requests.post: GET requests 1.622
    requests.head: HEAD requests 107
    requests.invalid: INVALID requests 6
*/
export interface LogInsights {

    requests_total: number;
    requests_get: number;
    requests_post: number;
    requests_head: number;
    requests_invalid: number;
    
}