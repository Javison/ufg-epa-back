import { HTTPRequest } from './http-request.interface';
import { LogDatetime } from './log-datetime.interface';

export interface LogRecord {
    host: string;
    datetime: LogDatetime;
    request: HTTPRequest;
    response_code: number;
    document_size: number;
    logline?: string; // For data traceability
}

/* 
Every log line should have: 
    1: host: Address URL or IP, 
    2: datetime: timestamp "[DD:HH:MM:SS]" EDT time (four hours behind GMT)
        2.1: request.day
        2.2: request.hour
        2.3: request.minute
        2.4: request.second 
        2.5: request.timestamp 
    3: request: "GET /Consumer.html HTTP/1.0"
        3.1: request.method: 
        3.2: request.url: 
        3.3: request.protocol: 
        3.4: request.protocol_version: 
    4: response_code: 200, 400,
    5: Bytes transfered: Bytes of resource delivered. ie: 1497, 
*/