
export class LogInsights {

    public requests_total: number | 0;
    public requests_get: number;
    public requests_post: number;
    public requests_head: number;
    public requests_invalid: number;

    constructor() {
        this.requests_total         =  0;
        this.requests_get           =  0;
        this.requests_post          =  0;  
        this.requests_head          =  0;  
        this.requests_invalid       =  0;  
    }
        
}