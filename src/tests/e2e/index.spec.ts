import firebaseFuntionsTest = require("firebase-functions-test");
import * as admin from "firebase-admin";
import * as path from "path";

const projectConfig = {
    projectId: "ufg-epa",
    storageBucket: 'ufg-epa.appspot.com',
    databaseURL: "http://localhost:9000/?ns=ufg-epa"
};

const testEnv = firebaseFuntionsTest(projectConfig, path.resolve("serviceAccountKey.json"));

describe('testing basic function', () =>{

    let index: any;
    let logFileUploadedOnCreateDbFn: any;
    let adminStub: jest.SpyInstance<admin.app.App, [(admin.AppOptions | undefined)?, (string | undefined)?]>;

    beforeAll( async () =>{
        adminStub = jest.spyOn(admin, 'initializeApp');
        index = require('../../index.ts'); 
        logFileUploadedOnCreateDbFn = require('../../fn/logFileUploadedOnCreateDbFn'); 
        console.log('index:', index);
        console.log('logFileUploadedOnCreateDbFn', logFileUploadedOnCreateDbFn);
    });
    
    afterAll( () =>{
        adminStub.mockRestore();
        testEnv.cleanup();
    });

    it('should instantiate mock environment',() =>{
        expect(true).toBeTruthy();
    });
    
});

