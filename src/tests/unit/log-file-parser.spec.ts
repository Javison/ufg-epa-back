import LogFileParser from '../../helpers/log-file-parser.helper';
import { LogDatetime } from '../../interfaces/log-datetime.interface';
import { HTTPRequest } from '../../interfaces/http-request.interface';

describe('LogFileParser Class', () => { 

  const line: string = '141.243.1.172 [29:23:53:25] "GET /Software.html HTTP/1.0" 200 1497';
  const line2: string = '152.35.128.13 [30:15:17:12] "GET /docs/CSO.html HTTP/1.0" 200 3120';
  const line3: string = 'jhws19.aca.ilstu.edu [30:23:53:07] "GET /cgi-bin/waisgate?port=210&ip_address=earth1&database_name=/usr1/comwais/indexes/HTDOCS&headline=%20EPA%20National%20GIS%20Program&type=HTML&docid=%01%0aearth1%3a210%02%1c%2fusr1%2fcomwais%2findexes%2fHTDOCS%03%2c0%20%2d5550%20%2fusr1%2fhttpd%2fhtdocs%2fngispr%2findex%2ehtml%04%0aearth1%3a210%05%1c%2fusr1%2fcomwais%2findexes%2fHTDOCS%06%2c0%20%2d5550%20%2fusr1%2fhttpd%2fhtdocs%2fngispr%2findex%2ehtml%07%01%00&seed_words_used=agency&byte_count=5550 HTTP/1.0" 200 5719';
  const lineInvalid: string = 'willard-ibmpc12.cac-labs.psu.edu [30:15:17:13] "cons/book.gif" 400 -';
  const lineRequestWithoutProtocol: string = 'willard-ibmpc12.cac-labs.psu.edu [30:15:17:11] "GET /" 200 4888';

  it('should parse host', () => {
    const host: string = LogFileParser.getHost(line);
    const host2: string = LogFileParser.getHost(line2);
    const host3: string = LogFileParser.getHost(line3);
    expect(host).toBe('141.243.1.172');
    expect(host2).toBe('152.35.128.13');
    expect(host3).toBe('jhws19.aca.ilstu.edu');
  })

  it('should parse LogDatetime', () => {
    const logDatetime: LogDatetime = LogFileParser.getLogdatetime(line);    // [29:23:53:25]
    const logDatetime2: LogDatetime = LogFileParser.getLogdatetime(line2);  // [30:15:17:12]
    const logDatetime3: LogDatetime = LogFileParser.getLogdatetime(line3);  // [30:23:53:07]

    const logDatetimeObj: LogDatetime = {
      year: 1995, 
      month: 7,   // 0 - 11 !!!
      day: 29, hour: 23, minute: 53, second: 25, timestamp: '1995-08-29T23:53:25.000-04:00', 
    };

    const logDatetimeObj2: LogDatetime = {
      year: 1995, 
      month: 7,   // 0 - 11 !!!
      day: 30, hour: 15, minute: 17, second: 12, timestamp: '1995-08-30T15:17:12.000-04:00', 
    };

    const logDatetimeObj3: LogDatetime = {
      year: 1995, 
      month: 7,   // 0 - 11 !!!
      day: 30, hour: 23, minute: 53, second: 7, timestamp: '1995-08-30T23:53:07.000-04:00', 
    };

    expect(logDatetime).toMatchObject(logDatetimeObj);
    expect(logDatetime2).toMatchObject(logDatetimeObj2);
    expect(logDatetime3).toMatchObject(logDatetimeObj3);
  });

  it('should parse HTTPRequest', () => {
    const httpRequest: HTTPRequest = LogFileParser.getLogHttpRequest(line); 
    const httpRequest2: HTTPRequest = LogFileParser.getLogHttpRequest(line2); 
    const httpRequest3: HTTPRequest = LogFileParser.getLogHttpRequest(line3); 
    
    const httpRequestObj: HTTPRequest = {
      method: 'GET', url: '/Software.html', protocol: 'HTTP', protocol_version: '1.0'
    };
    const httpRequestObj2: HTTPRequest = {
      method: 'GET', url: '/docs/CSO.html', protocol: 'HTTP', protocol_version: '1.0'
    };
    const httpRequestObj3: HTTPRequest = {
      method: 'GET', url: '/cgi-bin/waisgate?port=210&ip_address=earth1&database_name=/usr1/comwais/indexes/HTDOCS&headline=%20EPA%20National%20GIS%20Program&type=HTML&docid=%01%0aearth1%3a210%02%1c%2fusr1%2fcomwais%2findexes%2fHTDOCS%03%2c0%20%2d5550%20%2fusr1%2fhttpd%2fhtdocs%2fngispr%2findex%2ehtml%04%0aearth1%3a210%05%1c%2fusr1%2fcomwais%2findexes%2fHTDOCS%06%2c0%20%2d5550%20%2fusr1%2fhttpd%2fhtdocs%2fngispr%2findex%2ehtml%07%01%00&seed_words_used=agency&byte_count=5550', 
      protocol: 'HTTP', protocol_version: '1.0'
    };
    
    expect(httpRequest).toMatchObject(httpRequestObj);
    expect(httpRequest2).toMatchObject(httpRequestObj2);
    expect(httpRequest3).toMatchObject(httpRequestObj3);
  });

  it('should parse response code', () => {
    const response_code: number = LogFileParser.getHttpResponseCode(line);    
    expect(response_code).toBe(200);
  });

  it('should parse document size', () => {
    const document_size: number = LogFileParser.getServedDocumentSize(line);  
    expect(document_size).toBe(1497);
  });

  it('should parse HTTPRequest when is invalid', () => {
    const httpRequest: HTTPRequest = LogFileParser.getLogHttpRequest(lineInvalid); 
    const httpRequestObj: HTTPRequest = {
      method: 'invalid',
      url: '/',
      protocol: '',
      protocol_version: ''
    };

    expect(httpRequest).toMatchObject(httpRequestObj)
  });

  it('should parse document size as 0 when is invalid', () => {
    const document_size: number = LogFileParser.getServedDocumentSize(lineInvalid);  
    expect(document_size).toBe(0);
  });

  it('should parse correctly a request without protocol & protocol version', () => {
    // willard-ibmpc12.cac-labs.psu.edu [30:15:17:11] "GET /" 200 4888
    const response_code: number = LogFileParser.getHttpResponseCode(lineRequestWithoutProtocol);
    const document_size: number = LogFileParser.getServedDocumentSize(lineRequestWithoutProtocol);
    const httpRequest: HTTPRequest = LogFileParser.getLogHttpRequest(lineRequestWithoutProtocol); 
    const httpRequestObj: HTTPRequest = {
      method: 'GET',
      url: '/',
      protocol: '',
      protocol_version: ''
    };

    expect(response_code).toBe(200);
    expect(document_size).toBe(4888);
    expect(httpRequest).toMatchObject(httpRequestObj);
  });

  it('should identify a valid common log format', () => {
    // willard-ibmpc12.cac-labs.psu.edu [30:15:17:11] "GET /" 200 4888
    //  127.0.0.1 user-identifier frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326
    const isLogLine: boolean = LogFileParser.isLogLine(line);
    const isLogLineInvalidRequest: boolean = LogFileParser.isLogLine(lineInvalid);
    const isLogLineRequestWithoutProtocol: boolean = LogFileParser.isLogLine(lineRequestWithoutProtocol);

    expect(isLogLine).toBeTruthy();
    expect(isLogLineInvalidRequest).toBeTruthy();
    expect(isLogLineRequestWithoutProtocol).toBeTruthy();
    expect(isLogLineRequestWithoutProtocol).toBeTruthy();

    const isFakeLine: boolean = LogFileParser.isLogLine('willard-i.com [30:15: "GET /" 200 4888');
    const isEmptyLine: boolean = LogFileParser.isLogLine('');
    const isUnCompleteLine: boolean = LogFileParser.isLogLine('willard-.com [30:15:17:11] "GET 200 4888');
    const isUnCompleteLine2: boolean = LogFileParser.isLogLine('127.0.0.1 [30:15:17:11] 200 4888');

    expect(isFakeLine).not.toBeTruthy();
    expect(isEmptyLine).not.toBeTruthy();
    expect(isUnCompleteLine).not.toBeTruthy();
    expect(isUnCompleteLine2).not.toBeTruthy();

  });

});
